﻿using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple.Job
{
    public class JobSchedule
    {
        readonly bool IsInited = false;
        public JobSchedule(params Registry[] registrys)
        {
            try
            {
                if (registrys.Length == 0)
                {
                    registrys = new Registry[] { new JobRegistry() };
                }
                JobManager.Initialize(registrys);
                JobManager.JobEnd += JobManager_JobEnd; ;
                IsInited = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void JobManager_JobEnd(JobEndInfo obj)
        {
            JobEnd?.Invoke(obj);
        }

        public void Start()
        {
            JobManager.Start();
        }
        public void Stop()
        {
            JobManager.Stop();
        }

        public event Action<JobEndInfo> JobEnd;
    }
}
